import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {

    const [name, setName] = useState('')
    const [startDate, setStartDate] = useState(0)
    const [endDate, setEndDate] = useState('')
    const [descriptionField, setDescriptionField] = useState('')
    const [maxPres, setMaxPres] = useState(0)
    const [maxAttendees, setMaxAttendees] = useState(0)
    const [locations, setLocations] = useState([])
    const [location, setLocation] = useState(0)

    const handleNameChange= (event) => {
    const value = event.target.value
    setName(value)
    }

    const handleStartDateChange = (event) => {
        const value = event.target.value
        setStartDate(value)
    }

    const handleEndDateChange = (event) => {
        const value = event.target.value
        setEndDate(value)
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value
        console.log(value)
        setDescriptionField(value)
    }

    const handleMaxPresChange = (event) => {
        const value = event.target.value
        console.log(value)
        setMaxPres(value)
    }

    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value
        console.log(value)
        setMaxAttendees(value)
    }

    const handleLocChange = (event) => {
        const value = event.target.value
        console.log(value)
        setLocation(value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()

        if (!location || location.id === 0) {
            alert('Please select a valid location.')
            return
        }

        const data = {}

        data.starts = startDate
        data.name = name
        data.ends = endDate
        data.description = descriptionField
        data.max_presentations = maxPres
        data.max_attendees = maxAttendees
        data.location = location
        console.log(data)

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference)

            setName('')
            setStartDate('')
            setEndDate('')
            setDescriptionField('')
            setMaxPres('')
            setMaxAttendees('')
            setLocation('0')
            setLocations([])
            fetchData()
        }
    }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    try {
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        console.log(data)
        setLocations(data.locations);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form id="create-conference-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" onChange={handleNameChange} />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={startDate} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" onChange={handleStartDateChange} />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input value={endDate} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" onChange={handleEndDateChange} />
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <textarea value={descriptionField} placeholder="Description" required type="textarea" name="description" id="description" className="form-control" onChange={handleDescriptionChange}></textarea>
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input value={maxPres} placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" onChange={handleMaxPresChange} />
                <label htmlFor="max_presentations">Max Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input value={maxAttendees} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" onChange={handleMaxAttendeesChange}/>
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>
              <div className="mb-3">
                <label htmlFor="location" className="form-label">Location</label>
                <select onChange={handleLocChange} name="location" required id="location" className="form-select">
                    <option value="0">Select location</option>
                    {locations.map(location => <option key={location.id} value={location.id}> {location.name} </option>)}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  );
}

export default ConferenceForm;
