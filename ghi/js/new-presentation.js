function errorHandling() {
    return `
    <div class="alert alert-warning" role="alert">
        Something went wrong!
    </div>
    `;
}

async function loadConferences() {
    const conferencesUrl = 'http://localhost:8000/api/conferences/';
    const responseConferences = await fetch(conferencesUrl);

    if (responseConferences.ok) {
        const dataConferences = await responseConferences.json();
        const selectTag = document.getElementById('conference');

        for (let conference of dataConferences.conferences) {
            const option = document.createElement('option');
            option.value = conference.id;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
    }
}

window.addEventListener('DOMContentLoaded', async () => {
    await loadConferences();

    const formTag = document.getElementById('create-presentation-form');

    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);

        const presentationData = {
            presenter_name: formData.get('presenter_name'),
            presenter_email: formData.get('presenter_email'),
            company_name: formData.get('company_name'),
            title: formData.get('title'),
            synopsis: formData.get('synopsis'),
        };

        const conferenceId = formData.get('conference');
        const presentationsUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;

        const json = JSON.stringify(presentationData);

        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const responsePresentations = await fetch(presentationsUrl, fetchConfig);

        if (responsePresentations.ok) {
            formTag.reset();
            const newPresentation = await responsePresentations.json();
            console.log(newPresentation);
        }
    });
});
