function errorHandling() {
    return `
    <div class="alert alert-warning" role="alert">
        Something went wrong!
    </div>
    `;
}

async function loadLocations() {
    const locationsUrl = 'http://localhost:8000/api/locations/';
    const responseLocations = await fetch(locationsUrl);

    if (responseLocations.ok) {
        const dataLocations = await responseLocations.json();
        const selectTag = document.getElementById('location');

        for (let location of dataLocations.locations) {
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.appendChild(option);
        }
    }
}

window.addEventListener('DOMContentLoaded', async () => {
    await loadLocations();

    const formTag = document.getElementById('create-conference-form');

    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);

        const conferenceData = {
            name: formData.get('name'),
            starts: formData.get('starts'),
            ends: formData.get('ends'),
            description: formData.get('description'),
            max_presentations: formData.get('max_presentations'),
            max_attendees: formData.get('max_attendees'),
            location: formData.get('location'),
        };

        const conferencesUrl = 'http://localhost:8000/api/conferences/';

        const json = JSON.stringify(conferenceData);

        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        console.log('Conference Data:', conferenceData);
        const responseConferences = await fetch(conferencesUrl, fetchConfig);

        if (responseConferences.ok) {
            formTag.reset();
            const newConference = await responseConferences.json();
            console.log(newConference);
        }
    });
});
